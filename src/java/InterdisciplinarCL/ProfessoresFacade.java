/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarCL;

import InterdisciplinarMDL.Professores;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Lucas Diogo
 */
@Stateless
public class ProfessoresFacade extends AbstractFacade<Professores> {

    @PersistenceContext(unitName = "InterdisciplinarPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProfessoresFacade() {
        super(Professores.class);
    }
    
}
