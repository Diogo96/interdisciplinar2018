/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "etnias")
@NamedQueries({
    @NamedQuery(name = "Etnias.findAll", query = "SELECT e FROM Etnias e")
    , @NamedQuery(name = "Etnias.findByIdetnia", query = "SELECT e FROM Etnias e WHERE e.idetnia = :idetnia")
    , @NamedQuery(name = "Etnias.findByDescricao", query = "SELECT e FROM Etnias e WHERE e.descricao = :descricao")})
public class Etnias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_etnia")
    private Integer idetnia;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;

    public Etnias() {
    }

    public Etnias(Integer idetnia) {
        this.idetnia = idetnia;
    }

    public Integer getIdetnia() {
        return idetnia;
    }

    public void setIdetnia(Integer idetnia) {
        this.idetnia = idetnia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idetnia != null ? idetnia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Etnias)) {
            return false;
        }
        Etnias other = (Etnias) object;
        if ((this.idetnia == null && other.idetnia != null) || (this.idetnia != null && !this.idetnia.equals(other.idetnia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Etnias[ idetnia=" + idetnia + " ]";
    }
    
}
