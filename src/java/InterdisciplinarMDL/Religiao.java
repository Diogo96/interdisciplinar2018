/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "religiao")
@NamedQueries({
    @NamedQuery(name = "Religiao.findAll", query = "SELECT r FROM Religiao r")
    , @NamedQuery(name = "Religiao.findByIdreligiao", query = "SELECT r FROM Religiao r WHERE r.idreligiao = :idreligiao")
    , @NamedQuery(name = "Religiao.findByDescricao", query = "SELECT r FROM Religiao r WHERE r.descricao = :descricao")})
public class Religiao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_religiao")
    private Integer idreligiao;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;

    public Religiao() {
    }

    public Religiao(Integer idreligiao) {
        this.idreligiao = idreligiao;
    }

    public Integer getIdreligiao() {
        return idreligiao;
    }

    public void setIdreligiao(Integer idreligiao) {
        this.idreligiao = idreligiao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idreligiao != null ? idreligiao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Religiao)) {
            return false;
        }
        Religiao other = (Religiao) object;
        if ((this.idreligiao == null && other.idreligiao != null) || (this.idreligiao != null && !this.idreligiao.equals(other.idreligiao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Religiao[ idreligiao=" + idreligiao + " ]";
    }
    
}
