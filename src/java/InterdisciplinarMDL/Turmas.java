/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "turmas")
@NamedQueries({
    @NamedQuery(name = "Turmas.findAll", query = "SELECT t FROM Turmas t")})
public class Turmas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_turma")
    private Integer idturma;
    @Column(name = "Capacidade")
    private Integer capacidade;
    @Column(name = "Data_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datafim;
    @Column(name = "Data_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datainicio;
    @Size(max = 70)
    @Column(name = "Tema")
    private String tema;
    @JoinTable(name = "matricula", joinColumns = {
        @JoinColumn(name = "Id_turma", referencedColumnName = "Id_turma")}, inverseJoinColumns = {
        @JoinColumn(name = "Id_pessoa", referencedColumnName = "Id_pessoa")})
    @ManyToMany
    private Collection<Alunos> alunosCollection;
    @JoinColumn(name = "Id_curso", referencedColumnName = "Id_curso")
    @ManyToOne
    private Cursos idcurso;

    public Turmas() {
    }

    public Turmas(Integer idturma) {
        this.idturma = idturma;
    }

    public Integer getIdturma() {
        return idturma;
    }

    public void setIdturma(Integer idturma) {
        this.idturma = idturma;
    }

    public Integer getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(Integer capacidade) {
        this.capacidade = capacidade;
    }

    public Date getDatafim() {
        return datafim;
    }

    public void setDatafim(Date datafim) {
        this.datafim = datafim;
    }

    public Date getDatainicio() {
        return datainicio;
    }

    public void setDatainicio(Date datainicio) {
        this.datainicio = datainicio;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public Collection<Alunos> getAlunosCollection() {
        return alunosCollection;
    }

    public void setAlunosCollection(Collection<Alunos> alunosCollection) {
        this.alunosCollection = alunosCollection;
    }

    public Cursos getIdcurso() {
        return idcurso;
    }

    public void setIdcurso(Cursos idcurso) {
        this.idcurso = idcurso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idturma != null ? idturma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Turmas)) {
            return false;
        }
        Turmas other = (Turmas) object;
        if ((this.idturma == null && other.idturma != null) || (this.idturma != null && !this.idturma.equals(other.idturma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Turmas[ idturma=" + idturma + " ]";
    }
    
}
