/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "horarios")
@NamedQueries({
    @NamedQuery(name = "Horarios.findAll", query = "SELECT h FROM Horarios h")
    , @NamedQuery(name = "Horarios.findByIdhorario", query = "SELECT h FROM Horarios h WHERE h.idhorario = :idhorario")
    , @NamedQuery(name = "Horarios.findByDescricao", query = "SELECT h FROM Horarios h WHERE h.descricao = :descricao")})
public class Horarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_horario")
    private Integer idhorario;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;
    @JoinColumn(name = "Id_dia", referencedColumnName = "Id_dia")
    @ManyToOne
    private Dias iddia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "horarios")
    private Collection<HorarioPessoas> horarioPessoasCollection;

    public Horarios() {
    }

    public Horarios(Integer idhorario) {
        this.idhorario = idhorario;
    }

    public Integer getIdhorario() {
        return idhorario;
    }

    public void setIdhorario(Integer idhorario) {
        this.idhorario = idhorario;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Dias getIddia() {
        return iddia;
    }

    public void setIddia(Dias iddia) {
        this.iddia = iddia;
    }

    public Collection<HorarioPessoas> getHorarioPessoasCollection() {
        return horarioPessoasCollection;
    }

    public void setHorarioPessoasCollection(Collection<HorarioPessoas> horarioPessoasCollection) {
        this.horarioPessoasCollection = horarioPessoasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idhorario != null ? idhorario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horarios)) {
            return false;
        }
        Horarios other = (Horarios) object;
        if ((this.idhorario == null && other.idhorario != null) || (this.idhorario != null && !this.idhorario.equals(other.idhorario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Horarios[ idhorario=" + idhorario + " ]";
    }
    
}
