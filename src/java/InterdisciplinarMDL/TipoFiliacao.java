/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "tipo_filiacao")
@NamedQueries({
    @NamedQuery(name = "TipoFiliacao.findAll", query = "SELECT t FROM TipoFiliacao t")
    , @NamedQuery(name = "TipoFiliacao.findByIdtipo", query = "SELECT t FROM TipoFiliacao t WHERE t.idtipo = :idtipo")
    , @NamedQuery(name = "TipoFiliacao.findByDescricao", query = "SELECT t FROM TipoFiliacao t WHERE t.descricao = :descricao")})
public class TipoFiliacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_tipo")
    private Integer idtipo;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;
    @OneToMany(mappedBy = "idtipo")
    private Collection<Filiacao> filiacaoCollection;

    public TipoFiliacao() {
    }

    public TipoFiliacao(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public Integer getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Collection<Filiacao> getFiliacaoCollection() {
        return filiacaoCollection;
    }

    public void setFiliacaoCollection(Collection<Filiacao> filiacaoCollection) {
        this.filiacaoCollection = filiacaoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipo != null ? idtipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoFiliacao)) {
            return false;
        }
        TipoFiliacao other = (TipoFiliacao) object;
        if ((this.idtipo == null && other.idtipo != null) || (this.idtipo != null && !this.idtipo.equals(other.idtipo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.TipoFiliacao[ idtipo=" + idtipo + " ]";
    }
    
}
