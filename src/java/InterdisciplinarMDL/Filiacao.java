/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "filiacao")
@NamedQueries({
    @NamedQuery(name = "Filiacao.findAll", query = "SELECT f FROM Filiacao f")
    , @NamedQuery(name = "Filiacao.findByIdfiliacao", query = "SELECT f FROM Filiacao f WHERE f.idfiliacao = :idfiliacao")
    , @NamedQuery(name = "Filiacao.findByDescricao", query = "SELECT f FROM Filiacao f WHERE f.descricao = :descricao")})
public class Filiacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_filiacao")
    private Integer idfiliacao;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;
    @JoinColumn(name = "Id_tipo", referencedColumnName = "Id_tipo")
    @ManyToOne
    private TipoFiliacao idtipo;

    public Filiacao() {
    }

    public Filiacao(Integer idfiliacao) {
        this.idfiliacao = idfiliacao;
    }

    public Integer getIdfiliacao() {
        return idfiliacao;
    }

    public void setIdfiliacao(Integer idfiliacao) {
        this.idfiliacao = idfiliacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoFiliacao getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(TipoFiliacao idtipo) {
        this.idtipo = idtipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfiliacao != null ? idfiliacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Filiacao)) {
            return false;
        }
        Filiacao other = (Filiacao) object;
        if ((this.idfiliacao == null && other.idfiliacao != null) || (this.idfiliacao != null && !this.idfiliacao.equals(other.idfiliacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Filiacao[ idfiliacao=" + idfiliacao + " ]";
    }
    
}
