/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "dias")
@NamedQueries({
    @NamedQuery(name = "Dias.findAll", query = "SELECT d FROM Dias d")
    , @NamedQuery(name = "Dias.findByIddia", query = "SELECT d FROM Dias d WHERE d.iddia = :iddia")
    , @NamedQuery(name = "Dias.findByDescricao", query = "SELECT d FROM Dias d WHERE d.descricao = :descricao")})
public class Dias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_dia")
    private Integer iddia;
    @Size(max = 70)
    @Column(name = "Descricao")
    private String descricao;
    @OneToMany(mappedBy = "iddia")
    private Collection<Horarios> horariosCollection;

    public Dias() {
    }

    public Dias(Integer iddia) {
        this.iddia = iddia;
    }

    public Integer getIddia() {
        return iddia;
    }

    public void setIddia(Integer iddia) {
        this.iddia = iddia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Collection<Horarios> getHorariosCollection() {
        return horariosCollection;
    }

    public void setHorariosCollection(Collection<Horarios> horariosCollection) {
        this.horariosCollection = horariosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddia != null ? iddia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dias)) {
            return false;
        }
        Dias other = (Dias) object;
        if ((this.iddia == null && other.iddia != null) || (this.iddia != null && !this.iddia.equals(other.iddia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Dias[ iddia=" + iddia + " ]";
    }
    
}
