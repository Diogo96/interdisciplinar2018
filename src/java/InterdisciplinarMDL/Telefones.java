/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "telefones")
@NamedQueries({
    @NamedQuery(name = "Telefones.findAll", query = "SELECT t FROM Telefones t")
    , @NamedQuery(name = "Telefones.findByIdtelefone", query = "SELECT t FROM Telefones t WHERE t.idtelefone = :idtelefone")
    , @NamedQuery(name = "Telefones.findByTelefones", query = "SELECT t FROM Telefones t WHERE t.telefones = :telefones")})
public class Telefones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_telefone")
    private Integer idtelefone;
    @Size(max = 15)
    @Column(name = "Telefones")
    private String telefones;

    public Telefones() {
    }

    public Telefones(Integer idtelefone) {
        this.idtelefone = idtelefone;
    }

    public Integer getIdtelefone() {
        return idtelefone;
    }

    public void setIdtelefone(Integer idtelefone) {
        this.idtelefone = idtelefone;
    }

    public String getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefones) {
        this.telefones = telefones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtelefone != null ? idtelefone.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telefones)) {
            return false;
        }
        Telefones other = (Telefones) object;
        if ((this.idtelefone == null && other.idtelefone != null) || (this.idtelefone != null && !this.idtelefone.equals(other.idtelefone))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Telefones[ idtelefone=" + idtelefone + " ]";
    }
    
}
