/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lucas Diogo
 */
@Embeddable
public class HorarioPessoasPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_pessoa")
    private int idpessoa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_horario")
    private int idhorario;

    public HorarioPessoasPK() {
    }

    public HorarioPessoasPK(int idpessoa, int idhorario) {
        this.idpessoa = idpessoa;
        this.idhorario = idhorario;
    }

    public int getIdpessoa() {
        return idpessoa;
    }

    public void setIdpessoa(int idpessoa) {
        this.idpessoa = idpessoa;
    }

    public int getIdhorario() {
        return idhorario;
    }

    public void setIdhorario(int idhorario) {
        this.idhorario = idhorario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idpessoa;
        hash += (int) idhorario;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioPessoasPK)) {
            return false;
        }
        HorarioPessoasPK other = (HorarioPessoasPK) object;
        if (this.idpessoa != other.idpessoa) {
            return false;
        }
        if (this.idhorario != other.idhorario) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.HorarioPessoasPK[ idpessoa=" + idpessoa + ", idhorario=" + idhorario + " ]";
    }
    
}
