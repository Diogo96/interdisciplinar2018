/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "horario_pessoas")
@NamedQueries({
    @NamedQuery(name = "HorarioPessoas.findAll", query = "SELECT h FROM HorarioPessoas h")
    , @NamedQuery(name = "HorarioPessoas.findByIdpessoa", query = "SELECT h FROM HorarioPessoas h WHERE h.horarioPessoasPK.idpessoa = :idpessoa")
    , @NamedQuery(name = "HorarioPessoas.findByIdhorario", query = "SELECT h FROM HorarioPessoas h WHERE h.horarioPessoasPK.idhorario = :idhorario")
    , @NamedQuery(name = "HorarioPessoas.findByAno", query = "SELECT h FROM HorarioPessoas h WHERE h.ano = :ano")
    , @NamedQuery(name = "HorarioPessoas.findBySituacao", query = "SELECT h FROM HorarioPessoas h WHERE h.situacao = :situacao")})
public class HorarioPessoas implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HorarioPessoasPK horarioPessoasPK;
    @Column(name = "Ano")
    private Integer ano;
    @Size(max = 15)
    @Column(name = "Situacao")
    private String situacao;
    @JoinColumn(name = "Id_horario", referencedColumnName = "Id_horario", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Horarios horarios;

    public HorarioPessoas() {
    }

    public HorarioPessoas(HorarioPessoasPK horarioPessoasPK) {
        this.horarioPessoasPK = horarioPessoasPK;
    }

    public HorarioPessoas(int idpessoa, int idhorario) {
        this.horarioPessoasPK = new HorarioPessoasPK(idpessoa, idhorario);
    }

    public HorarioPessoasPK getHorarioPessoasPK() {
        return horarioPessoasPK;
    }

    public void setHorarioPessoasPK(HorarioPessoasPK horarioPessoasPK) {
        this.horarioPessoasPK = horarioPessoasPK;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Horarios getHorarios() {
        return horarios;
    }

    public void setHorarios(Horarios horarios) {
        this.horarios = horarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (horarioPessoasPK != null ? horarioPessoasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorarioPessoas)) {
            return false;
        }
        HorarioPessoas other = (HorarioPessoas) object;
        if ((this.horarioPessoasPK == null && other.horarioPessoasPK != null) || (this.horarioPessoasPK != null && !this.horarioPessoasPK.equals(other.horarioPessoasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.HorarioPessoas[ horarioPessoasPK=" + horarioPessoasPK + " ]";
    }
    
}
