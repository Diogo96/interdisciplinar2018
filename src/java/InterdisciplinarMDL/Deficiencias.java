/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Lucas Diogo
 */
@Entity
@Table(name = "deficiencias")
@NamedQueries({
    @NamedQuery(name = "Deficiencias.findAll", query = "SELECT d FROM Deficiencias d")})
public class Deficiencias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_deficencia")
    private Integer iddeficencia;
    @Size(max = 15)
    @Column(name = "Descricao")
    private String descricao;
    @Column(name = "Grau")
    private Integer grau;

    public Deficiencias() {
    }

    public Deficiencias(Integer iddeficencia) {
        this.iddeficencia = iddeficencia;
    }

    public Integer getIddeficencia() {
        return iddeficencia;
    }

    public void setIddeficencia(Integer iddeficencia) {
        this.iddeficencia = iddeficencia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getGrau() {
        return grau;
    }

    public void setGrau(Integer grau) {
        this.grau = grau;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddeficencia != null ? iddeficencia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deficiencias)) {
            return false;
        }
        Deficiencias other = (Deficiencias) object;
        if ((this.iddeficencia == null && other.iddeficencia != null) || (this.iddeficencia != null && !this.iddeficencia.equals(other.iddeficencia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.Deficiencias[ iddeficencia=" + iddeficencia + " ]";
    }
    
}
