/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterdisciplinarMDL;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lucas Diogo
 */
@Embeddable
public class ItemVendaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_item")
    private int iditem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Id_venda")
    private int idvenda;

    public ItemVendaPK() {
    }

    public ItemVendaPK(int iditem, int idvenda) {
        this.iditem = iditem;
        this.idvenda = idvenda;
    }

    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public int getIdvenda() {
        return idvenda;
    }

    public void setIdvenda(int idvenda) {
        this.idvenda = idvenda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) iditem;
        hash += (int) idvenda;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemVendaPK)) {
            return false;
        }
        ItemVendaPK other = (ItemVendaPK) object;
        if (this.iditem != other.iditem) {
            return false;
        }
        if (this.idvenda != other.idvenda) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InterdisciplinarMDL.ItemVendaPK[ iditem=" + iditem + ", idvenda=" + idvenda + " ]";
    }
    
}
