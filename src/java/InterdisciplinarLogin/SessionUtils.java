package InterdisciplinarLogin;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 *
 * @author Estéfani Evangelista
 */
public class SessionUtils {
    public static HttpSession getSession() {
		return (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}

	public static String getLogin() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(false);
		return session.getAttribute("login").toString();
	}

	public static String getIdPessoa() {
		HttpSession session = getSession();
		if (session != null)
			return (String) session.getAttribute("id_pessoa");
		else
			return null;
	}
}
